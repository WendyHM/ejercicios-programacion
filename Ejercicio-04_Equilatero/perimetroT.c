#include <stdio.h>

/* Función que calcula el perímetro de un triángulo equilatero.*/
int main()
{
	double lado = 0;
	double perimetro = 0;

	printf("Ingresa el valor de un lado del triángulo equilátero: ");
	scanf("%lf", &lado);
    if( 0 < lado ){
		perimetro = lado * 3;
		printf("El perímetro es: %.2f\n", perimetro);
		return 0;
	}else{
		printf("El valor del lado del triángulo debe ser positivo.\n");
		return 0;
	}
}