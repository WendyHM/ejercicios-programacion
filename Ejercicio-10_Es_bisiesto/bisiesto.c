#include <stdio.h>
#include <stdbool.h>

/* Función que calcula si un año es bisiesto o no. */
int main()
{
	int year = 0;
	bool esBisiesto = false;

	printf("Ingrese el valor de un año:");
	scanf("%i", &year);
	esBisiesto = ( year % 4 == 0 ) ? true : false;
	if( year % 100 == 0 ){
		esBisiesto = false;
		if( year % 400 == 0 ){
			esBisiesto = true;
		}
	} 
	if( esBisiesto ){
		printf("El año ingresado es bisiesto\n");
	}else{
		printf("El año ingresado no es bisiesto.\n");
	}
	return 0;
}