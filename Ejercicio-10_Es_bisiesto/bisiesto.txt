Inicio
	Variable year es Numérico Natural
	Variable esBisiesto es booleana

	year = 0
	esBisiesto = false

	Leer "Ingrese el valor de un año: ", year
	esBisiesto =  ( year % 4 == 0 ) ? true : false
	Si ( year % 100 == 0 )entonces
		esBisiesto = false
		Si ( year % 400 == 0 ) entonces
			esBisiesto = false
		Fin-Si
	Fin-Si
	Si ( esBisiesto ) entonces
		Escribir "El año ingresado es bisiesto."
	Sino
		Escribir "El año ingresado no es bisiesto."
	Fin-Si
Fin