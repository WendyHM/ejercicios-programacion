#include <stdio.h>

/* Función que imprime el nombre de un mes, dependiendo de la
   opción ingresada. Los valores van de 0 a 11.*/
int main()
{
	int opcion = -1;

	printf("Ingresa un número entre 0 y 11: ");
	scanf("%i", &opcion);
	switch (opcion){
		case 0:
			printf("Enero\n");
			break;
		case 1:
			printf("Febrero\n");
			break;
		case 2:
			printf("Marzo\n");
			break;
		case 3:
			printf("Abril\n");
			break;
		case 4:
			printf("Mayo\n");
			break;
		case 5:
			printf("Junio\n");
			break;
		case 6:
			printf("Julio\n");
			break;
		case 7:
			printf("Agosto\n");
			break;
		case 8:
			printf("Septiembre\n");
			break;
		case 9:
			printf("Octubre\n");
			break;
		case 10:
			printf("Noviembre\n");
			break;
		case 11:
			printf("Diciembre\n");
			break;
		default:
			printf("La opción no es válida.\n");
			break;
	}
	return 0;
}