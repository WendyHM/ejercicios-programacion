#include <stdio.h>

/* Función que imprime la tabla de multiplicar de un número desde 2 a 10. */
int main()
{
	double numero = 0;
	double auxiliar = 0;

	printf("Ingresa un número:");
	scanf("%lf", &numero);
	printf("Su tabla de multiplicar es:\n");
	for( int i = 2 ; i <= 10 ; i++ ){
		auxiliar = numero * i;
		printf("%.1f * %d = %.1f \n", numero, i, auxiliar);
	}
	return 0;
}