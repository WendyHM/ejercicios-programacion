#include <stdio.h>

/* Esta función convierte una cantidad de dólares a pesos mexicanos */
int main()
{
    double dolar = 0;
    double peso = 0;

    printf("Ingresa el monto en dólares: ");
    scanf("%lf", &dolar);
    if( 0 < dolar ){
        peso = dolar * 22.0597; //Venta de dólar 17/08/2020
        printf("%.2f USD = %.4f MXM\n", dolar, peso);
        return 0;
    }else{
        printf("El monto debe ser mayor a cero.\n");
        return 0;
    }
}
