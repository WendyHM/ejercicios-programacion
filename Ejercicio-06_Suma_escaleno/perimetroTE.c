#include <stdio.h>

/* Función que calcula el perímetro de un triángulo. */
int main(){
	double lado1 = 0;
	double lado2 = 0;
	double lado3 = 0;
	double perimetro = 0;

	printf("Ingrese el valor del primer lado: ");
	scanf("%lf", &lado1);
	printf("Ingrese el valor del segundo lado: ");
	scanf("%lf", &lado2);
	printf("Ingrese el valor del tercer lado: ");
	scanf("%lf", &lado3);
	if( 0 < lado1 && 0 < lado2 && 0 < lado3 ){
		if( lado1 < lado2 + lado3 && lado2 < lado1 + lado3 && lado3 < lado1 + lado2 ){
			perimetro = lado1 + lado2 + lado3;
			printf("El perímetro es: %.2f\n", perimetro);
			return 0;
		}else{
			printf("Los valores ingresados no corresponden con la descripción de un triángulo.\n");
		}
	}else{
		printf("Los valores ingresados deben de ser mayores a cero.\n");
	}
	return 0;
}