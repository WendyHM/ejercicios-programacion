#include <stdio.h>

/* Función que calcula el perímetro de un triángulo isósceles. */
int main(){
	double lado1 = 0;
	double lado2 = 0;
	double perimetro = 0;

	printf("Ingresa el valor del lado diferente: ");
	scanf("%lf", &lado1);
	printf("Ingrese el valor de uno de los lados iguales: ");
	scanf("%lf", &lado2);
	if( 0 < lado1 && 0 < lado2 ){
		if( lado1 < 2 * lado2 ){
			perimetro = lado1 + lado2 * 2;
			printf("El perimetro es: %.2f\n", perimetro);
			return 0;
		}else{
			printf("Los valores ingresados no cumplen con la descripción de un triángulo.\n");
			return 0;
		}
	}else{
		printf("Los valores de los lados deben de ser mayores a cero. \n");
	}
}