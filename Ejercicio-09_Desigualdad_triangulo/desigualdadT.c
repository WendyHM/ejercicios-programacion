#include <stdio.h>

/* Función que determina si se puede formar un 
   triángulo con los valores ingresados */
int main()
{
	double lado1 = 0;
	double lado2 = 0;
	double lado3 = 0;

	printf("Ingrese la primer longitud:");
	scanf("%lf", &lado1);
	printf("Ingrese la segunda longitud:");
	scanf("%lf", &lado2);
	printf("Ingrese la tercera longitud:");
	scanf("%lf", &lado3);
	if( 0 < lado1 && 0 < lado2 && 0 < lado3 ){
		if( lado1<lado2+lado3 && lado2<lado1+lado3 && lado3<lado1+lado2 ){
			printf("Es posible formar un triángulo con esos segmentos. \n");
		}else{
			printf("No es posible formar un triángulo con los segmentos dados.\n");
		}
	}else{
		printf("No es posible formar un triángulo con los segmentos dados.\n");
	}
	return 0;
}