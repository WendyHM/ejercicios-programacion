#include <stdio.h>

/*Función que calcula la suma de los primeros n números en el rango de 1 a 50*/
int main()
{
    // Lo usamos para para saber si el valor ingresado es un número entero.
    float verificador;

    int numero;
    int suma;

    printf("Ingresa un número: ");
    scanf("%f",&verificador);

    /* El valor de verificador se convierte a entero si es un número,
      si es un caracter se convierte a 0 */
    numero = (int) verificador;

    /* Si el valor de las dos variables es igual entonces el número
       ingresado siempre fue un número entero y pasa al siguiente if*/
    if( verificador != numero ){
        printf("Debes ingresar un número entero en el rango de 1 a 50.\n");
        return 0;
    }
    
    if( 1 <= numero && numero <= 50 )
    {
        suma = (numero * (numero + 1))/2;
        printf("La suma de los primeros %i números es: %i\n", numero, suma);
        return 0;
    }
    else{
        printf("El número debe estar en el rango de 1 a 50\n");
    }
}
