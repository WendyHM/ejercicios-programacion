#include <stdio.h>

/* Función que calcula el perímetro de un triángulo, pregunta
a qué tipo de triángulo se quiere calcular su perímetro. */

int main(){
	int opcion = 0;
	double lado1 = 0;
	double lado2 = 0;
	double lado3 = 0;
	double perimetro = 0;
	char mensaje_negativos[] = "El valor de los lados de un triángulo debe ser mayor a cero.";
	char mensaje_triangulo[] = "Los valores ingresados no corresponden con la descripción de un triángulo.";

	printf("**********Ingrese el valor de la opción que desea calcular:**********\n");
	printf("1  -Perímetro de triángulo equilátero\n");
	printf("2  -Perímetro de triángulo isósceles\n");
	printf("3  -Perímetro de triángulo escaleno\n");
	scanf("%i", &opcion);
	switch(opcion){
		case 1:
            printf("Ingresa el valor de un lado del triángulo equilátero: ");
			scanf("%lf", &lado1);
   			if( 0 < lado1 ){
				perimetro = lado1 * 3;
			}else{
				printf("%s\n", mensaje_negativos);
				return 0;
			}
		    break;
		case 2:
			printf("Ingresa el valor del lado diferente: ");
			scanf("%lf", &lado1);
			printf("Ingrese el valor de uno de los lados iguales: ");
			scanf("%lf", &lado2);
			if( 0 < lado1 && 0 < lado2 ){
				if( lado1 < 2 * lado2 ){
					perimetro = lado1 + lado2 * 2;
				}else{
					printf("%s\n", mensaje_triangulo);
					return 0;
				}
			}else{
				printf("%s\n", mensaje_negativos);
				return 0;
			}
			break;
		case 3:
			printf("Ingrese el valor del primer lado: ");
			scanf("%lf", &lado1);
			printf("Ingrese el valor del segundo lado: ");
			scanf("%lf", &lado2);
			printf("Ingrese el valor del tercer lado: ");
			scanf("%lf", &lado3);
			if( 0 < lado1 && 0 < lado2 && 0 < lado3 ){
				if( lado1 < lado2 + lado3 && lado2 < lado1 + lado3 && lado3 < lado1 + lado2 ){
					perimetro = lado1 + lado2 + lado3;
				}else{
					printf("%s\n", mensaje_triangulo);
					return 0;
				}
			}else{
				printf("%s\n", mensaje_negativos);
				return 0;
			}
			break;
		default:
			printf("El valor ingresado no corresponde con ninguna opción.\n");
			return 0;
	}
	printf("El perímetro es: %.2f\n", perimetro);
	return 0;
}