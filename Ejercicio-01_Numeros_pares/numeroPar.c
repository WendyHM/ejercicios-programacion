#include <stdio.h>

/*Función que calcula los números pares del 0 al 100*/
int main()
{
    int numeroPar;
    int conteo = 1;

    while(numeroPar <= 100)
    {
        printf("%i, ",numeroPar);
        numeroPar = 2 * conteo;
        conteo++;
    }
    printf("\n");
}
